﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GetSQLFromSourceCode
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
            TxtSourceCode.Text = @"D:\Working\FSOFT\FSU17.BU2\Project\2017\HSE-Conv\HSE-Conv-master-496973ecfdcc0b000dcc0eed4546dca1036737a6\Source\After_Convert\DWSGMM";
            TxtExtensions.Text = "vb";
            TxtIgnore.Text = "Designer.vb;AssemblyInfo.vb";
            TxtEncoding.Text = "shift_jis";
        }

        private void BtnSelectSource_Click(object sender, EventArgs e)
        {
            DialogResult result = DlgFolder.ShowDialog();
            if (result == DialogResult.OK)
            {
                TxtSourceCode.Text = DlgFolder.SelectedPath;
            }
        }

        private void BtnReadSourceCode_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(TxtSourceCode.Text))
            {
                MessageBox.Show("Please select folder source code!");
                return;
            }
            if (!Directory.Exists(TxtSourceCode.Text))
            {
                MessageBox.Show("Folder source code not exist!");
                return;
            }
            List<FileInfo> allFiles = GetAllFile(new DirectoryInfo(TxtSourceCode.Text), TxtExtensions.Text.Replace(".", "").ToLower().Split(';').ToList(), TxtIgnore.Text.ToLower().Split(';').ToList());
            Dictionary<string, List<string>> classess = GetAllClass(allFiles);
            Dictionary<string, List<string>> properties = GetAllProperty(classess);
            Dictionary<string, List<string>> methods = GetAllMethods(classess);
        }

        private Dictionary<string, List<string>> GetAllClass(List<FileInfo> AllFiles)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            Dictionary<string, string> contents = new Dictionary<string, string>();
            foreach (FileInfo file in AllFiles)
            {
                string fullName = file.FullName;
                string[] content = File.ReadAllLines(fullName, Encoding.GetEncoding(TxtEncoding.Text));
                for (int i = 0; i < content.Length; i++)
                {
                    contents.Add(string.Format("{0}({1})", file.FullName, i + 1), content[i]);
                }
            }
            int classStart = -1;
            StringBuilder builder = new StringBuilder("");
            string currentClass = "";
            string lineStart = "";
            string lineEnd = "";
            foreach (KeyValuePair<string, string> content in contents)
            {
                string value = content.Value.Trim();
                while (value.Contains("  "))
                {
                    value = value.Replace("  ", " ");
                }
                if (value.Contains("'"))
                {
                    int index = value.IndexOf("'");
                    value = value.Substring(0, index);
                }
                value = value.Trim();
                if (value.StartsWith("End Class") || value.Contains(" End Class") ||
                    value.StartsWith("End Module") || value.Contains(" End Module"))
                {
                    lineEnd = content.Key;
                    builder.Append(content.Value);
                    result.Add(currentClass, new List<string>() { builder.ToString(), lineStart, lineEnd });
                    builder = new StringBuilder("");
                    currentClass = "";
                    classStart = -1;
                }
                if (value.StartsWith("Class ") || value.Contains(" Class ") ||
                    value.StartsWith("Module ") || value.Contains(" Module "))
                {
                    List<string> str = value.Split(' ').ToList();
                    int index = str.IndexOf("Class") + str.IndexOf("Module") + 1;
                    if (str.Count > index)
                    {
                        classStart = 1;
                        lineStart = content.Key;
                        currentClass = str[index + 1];
                        builder.Append(content.Value);
                    }
                    else
                    {
                        classStart = -1;
                    }
                }
                if (classStart == 1)
                {
                    builder.Append(content.Value);
                }
            }
            return result;
        }

        private Dictionary<string, List<string>> GetAllProperty(Dictionary<string, List<string>> Classess)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            return result;
        }

        private Dictionary<string, List<string>> GetAllMethods(Dictionary<string, List<string>> Classess)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            string currentClass = "";
            foreach (KeyValuePair<string, List<string>> clazz in Classess)
            {

            }
            return result;
        }

        private List<FileInfo> GetAllFile(DirectoryInfo Folder, List<string> Extensions, List<string> Ignores)
        {
            if (!Folder.Exists)
            {
                return new List<FileInfo>();
            }
            List<FileInfo> result = new List<FileInfo>();
            DirectoryInfo[] folders = Folder.GetDirectories();
            if (null != Extensions && Extensions.Count > 0)
            {
                foreach (FileInfo childFile in Folder.GetFiles())
                {
                    bool ignore = false;
                    foreach (string Ignore in Ignores)
                    {
                        if (childFile.Name.ToLower().Contains(Ignore))
                        {
                            ignore = true;
                            break;
                        }
                    }
                    if (ignore)
                    {
                        continue;
                    }
                    if (Extensions.Contains(childFile.Extension.Replace(".", "").ToLower()))
                    {
                        result.Add(childFile);
                    }
                }
            }
            else
            {
                result.AddRange(Folder.GetFiles());
            }
            foreach (DirectoryInfo childFolder in folders)
            {
                result.AddRange(GetAllFile(childFolder, Extensions, Ignores));
            }
            return result;
        } 

    }
}
