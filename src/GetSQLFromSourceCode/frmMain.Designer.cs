﻿namespace GetSQLFromSourceCode
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxtSourceCode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BtnSelectSource = new System.Windows.Forms.Button();
            this.DlgFile = new System.Windows.Forms.OpenFileDialog();
            this.DlgSaveFile = new System.Windows.Forms.SaveFileDialog();
            this.DlgFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.BtnReadSourceCode = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtExtensions = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtEncoding = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtIgnore = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TxtSourceCode
            // 
            this.TxtSourceCode.Location = new System.Drawing.Point(110, 19);
            this.TxtSourceCode.Name = "TxtSourceCode";
            this.TxtSourceCode.Size = new System.Drawing.Size(404, 22);
            this.TxtSourceCode.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Source code:";
            // 
            // BtnSelectSource
            // 
            this.BtnSelectSource.Location = new System.Drawing.Point(520, 12);
            this.BtnSelectSource.Name = "BtnSelectSource";
            this.BtnSelectSource.Size = new System.Drawing.Size(85, 37);
            this.BtnSelectSource.TabIndex = 2;
            this.BtnSelectSource.Text = "Select";
            this.BtnSelectSource.UseVisualStyleBackColor = true;
            this.BtnSelectSource.Click += new System.EventHandler(this.BtnSelectSource_Click);
            // 
            // DlgFile
            // 
            this.DlgFile.FileName = "dlgFile";
            // 
            // DlgSaveFile
            // 
            this.DlgSaveFile.FileName = "dlgSaveFile";
            // 
            // BtnReadSourceCode
            // 
            this.BtnReadSourceCode.Location = new System.Drawing.Point(15, 204);
            this.BtnReadSourceCode.Name = "BtnReadSourceCode";
            this.BtnReadSourceCode.Size = new System.Drawing.Size(85, 37);
            this.BtnReadSourceCode.TabIndex = 3;
            this.BtnReadSourceCode.Text = "Read";
            this.BtnReadSourceCode.UseVisualStyleBackColor = true;
            this.BtnReadSourceCode.Click += new System.EventHandler(this.BtnReadSourceCode_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Extensions:";
            // 
            // TxtExtensions
            // 
            this.TxtExtensions.Location = new System.Drawing.Point(110, 62);
            this.TxtExtensions.Name = "TxtExtensions";
            this.TxtExtensions.Size = new System.Drawing.Size(404, 22);
            this.TxtExtensions.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(520, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 37);
            this.button1.TabIndex = 6;
            this.button1.Text = "Select";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(520, 55);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 37);
            this.button2.TabIndex = 7;
            this.button2.Text = "Select";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Encoding:";
            // 
            // TxtEncoding
            // 
            this.TxtEncoding.Location = new System.Drawing.Point(110, 148);
            this.TxtEncoding.Name = "TxtEncoding";
            this.TxtEncoding.Size = new System.Drawing.Size(404, 22);
            this.TxtEncoding.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Ignore:";
            // 
            // TxtIgnore
            // 
            this.TxtIgnore.Location = new System.Drawing.Point(110, 105);
            this.TxtIgnore.Name = "TxtIgnore";
            this.TxtIgnore.Size = new System.Drawing.Size(404, 22);
            this.TxtIgnore.TabIndex = 11;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(520, 98);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(85, 37);
            this.button3.TabIndex = 10;
            this.button3.Text = "Select";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 253);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtIgnore);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxtEncoding);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TxtExtensions);
            this.Controls.Add(this.BtnReadSourceCode);
            this.Controls.Add(this.BtnSelectSource);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxtSourceCode);
            this.Name = "FrmMain";
            this.Text = "Get SQL from source code";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxtSourceCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BtnSelectSource;
        private System.Windows.Forms.OpenFileDialog DlgFile;
        private System.Windows.Forms.SaveFileDialog DlgSaveFile;
        private System.Windows.Forms.FolderBrowserDialog DlgFolder;
        private System.Windows.Forms.Button BtnReadSourceCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxtExtensions;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtEncoding;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtIgnore;
        private System.Windows.Forms.Button button3;
    }
}

